﻿module.exports = {
	getRect: getRect
};

var _ = require('underscore');

function getRect(figure) {
	switch (figure) {
		case 'I':
			return {f: [
				[' '],
				[' '],
				[' '],
				[' ']
			], center: [0, -2, 0, -1]};
		case 'O':
			return { f: [
				[' ', ' '],
				[' ', ' ']
			], center: [0, 0, 0, 0]};
		case 'L':
			return { f: [
				[' '],
				[' '],
				[' ', ' ']
			], center: [0, -1, -1, -1]
			};
		case 'J':
			return { f: [
				['', ' '],
				['', ' '],
				[' ', ' ']
			], center: [-1, -1, 0, -1]
			};
		case 'S':
			return { f: [
				['', ' ', ' '],
				[' ', ' ', '']
			], center: [-1, 0, -1, 0]
			};
		case 'Z':
			return { f: [
				[' ', ' ', ''],
				['', ' ', ' ']
			], center: [-1, 0, -1, 0]
			};
		case 'T':
			return { f: [
				['', ' ', ''],
				[' ', ' ', ' ']
			], center: [-1, 0, -1, -1]
			};
		default:
			return { f: [
				[' ', ' '],
				[' ', ' ']
			], center: [0,0,0,0]
			};
	}
}
