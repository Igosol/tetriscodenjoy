﻿module.exports = Path;

var _ = require('underscore');
//var math = require('math');

function Path() {
	Path.prototype.leftPriotity = true;
}

Path.prototype.buildCommand = function (position, x, y, rect) {
	var self = this;
	
	self.position = position;
	
	if (!self.position.place || self.position.place.length <= 0) {
		return '';
	}

	var results = [];

	x = x + rect.center[position.turns];

	results.push(GetRotateCommands.call(self));
	results.push(GetXCommands.call(self, x));
	results.push(GetYCommands.call(self, y));
	
	Path.prototype.leftPriotity = !Path.prototype.leftPriotity;

	return results.join(', ');
}

function GetXCommands(x) {
	//var targetX = _.first(this.position.place);
	var targetX = Path.prototype.leftPriotity ? _.first(this.position.place) : _.last(this.position.place);

	var diffX = targetX - x;

	if (diffX == 0) {
		return '';
	}

	var directionX = diffX > 0 ? 'right' : 'left';

	return directionX + '=' + Math.abs(diffX);
}

function GetRotateCommands() {
	if (this.position.turns == 0) {
		return '';
	}

	return 'rotate=' + this.position.turns;
}

function GetYCommands(y) {
	return 'drop';
}
