var Field = require('./Field.js');
var Path = require('./Path.js');
var Figures = require('./Figures.js');
var path = new Path();

var log = function (string) {
	console.log(string);
};

var hostIp = 'ECSC001014B4';
//var hostIp = 'tetrisj.jvmhost.net';

var userName = 'IgorS';
var protocol = 'WS';

if (protocol == 'HTTP') {
	var http = require('http');
	var url = require('url');
	
	http.createServer(function (request, response) {
		var p = url.parse(request.url, true).query;
		
		var result = answer(p.figure, p.x, p.y, p.glass, p.next);
		log("submitted: " + result);
		
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end(result);
	}).listen(8888, hostIp);
	
	log('Server running at http://' + hostIp + ':8888/');
} else {
	var port = 8080;
	if (hostIp == 'tetrisj.jvmhost.net') {
		port = 12270;
	}
	
	var server = 'ws://' + hostIp + ':' + port + '/tetris-contest/ws';
	var WebSocket = require('ws');
	var ws = new WebSocket(server + '?user=' + userName);
	
	ws.on('open', function () {
		log('Opened');
	});
	
	ws.on('close', function () {
		log('Closed');
	});
	
	ws.on('message', function (message) {
		//log('received: ' + message);
		
		var pattern = new RegExp(/^figure=(\w+)&x=(\d+)&y=(\d+)&glass=(.*)&next=(\w*)$/);
		var parameters = message.match(pattern);
		
		var result = answer(parameters[1], parameters[2], parameters[3], parameters[4], parameters[5]);
		//log("submitted: " + result);
		
		ws.send(result);
	});
	
	log('Web socket client running at ' + server);
}

var Element = {
         // из этих символов состоит строка glass
	EMPTY : ' ',        // так выглядит свободное место в стакане
	BUSY : '*'          // а тут уже занято
}

var DO_NOT_ROTATE = 0;              // не вращать фигурку
var ROTATE_90_CLOCKWISE = 1;        // повернуть по часовой стрелке один раз
var ROTATE_180_CLOCKWISE = 2;       // повернуть по часовой стрелке два раза
var ROTATE_90_COUNTERCLOCKWISE = 3; // повернуть против часовой стрелки 1 раз (3 по часовой)

// метод, говорящий что делать той или иной фигурке figure с координарами x,y в стакане glass. next - очередь следущих фигурок
function answer(figure, x, y, glass, next) {
	var xx = parseInt(x);
	var yy = parseInt(y);


	var field = new Field(glass);
	//log(figure);

	var rect = Figures.getRect(figure);
	//log(rect.length);

	var position = field.GetFreeSpace(rect);
	var command = path.buildCommand(position, xx, yy, rect);

	log(command);

	return command;
	//return "right=2, drop";
}



