﻿module.exports = Field;

var _ = require('underscore'),
mathjs = require('mathjs');

function Field(glass) {
	this.rows = [];

	for (var i = 0; i < 20; i++) {
		this.rows.push(new FieldRow(glass.substring(i * 10, (i + 1) * 10)));
	}
}

function FieldRow(row) {
	this.rowStr = row;
	this.isFull = true;

	for (var i = 0; i < 10; i++) {
		if (row[i] === ' ') this.isFull = false;
	}
}

FieldRow.prototype.getFitMap = function (rectRow) {
	var self = this;
	
	if (self.isFull) return [];

	var map = [];
	
	for (var i = 0; i <= 10 - rectRow.length; i++) {
		var result = true;

		for (var j = 0; j < rectRow.length; j++) {
			if (self.rowStr[i + j] == '*' && rectRow[j] == ' ') {
				result = false;
			}
		}

		if (result) {
			map.push(i);
		}
	}

	return map;
}

Field.prototype.GetFreeSpace = function (rectObj) {
	var self = this;

	var rect = rectObj.f;

	var result = {
		turns: 0
	};
	
	for (var j = 0; j < 3; j++) {
		var rowsInvolved = rect.length;

		for (var i = 0; i <= self.rows.length - rowsInvolved; i++) {
			var maps = [];

			for (var l = 0; l < rect.length; l++) {
				maps.push(self.rows[i].getFitMap(rect[l]));
			}

			var control = rect.length;
			for (var k = 0; k < maps.length; k++) {
				if (maps[k].length > 0)
					control--;
			}
			
			if (control != 0) {
				continue;
			}

			var place = _.intersection.apply(null, maps);

			if (place.length > 0) {
				result.place = place;
				result.firstRow = i;
				return result;
			}
		}

		rect = mathjs.transpose(rect);
		rect.turns++;
	}

	return result;
}

